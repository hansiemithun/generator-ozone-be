# generator-ozone-be [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

> Create express-js app with jest bundled along with lint and pre-commit hooks configured

## Installation

First, install [Yeoman](http://yeoman.io) and generator-ozone-be using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-ozone-be
```

Then generate your new project:

```bash
yo ozone-be
```

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Mithun H A](https://www.npmjs.com/~hansiemithun)

[npm-image]: https://badge.fury.io/js/generator-ozone-be.svg
[npm-url]: https://npmjs.org/package/generator-ozone-be
[travis-image]: https://travis-ci.org/hansiemithun/generator-ozone-be.svg?branch=master
[travis-url]: https://travis-ci.org/hansiemithun/generator-ozone-be
[daviddm-image]: https://david-dm.org/hansiemithun/generator-ozone-be.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/hansiemithun/generator-ozone-be
[coveralls-image]: https://coveralls.io/repos/hansiemithun/generator-ozone-be/badge.svg
[coveralls-url]: https://coveralls.io/r/hansiemithun/generator-ozone-be
