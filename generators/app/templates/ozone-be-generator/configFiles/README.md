# Express SuperTest moxios testing

Testing an Express app with SuperTest and moxios

## Running the app

`npm run server` (It runs app with dev mode watching the server)
`npm run server:prod` (It runs app with dev but doesnt watch the server)

## Running tests

Node Version: `>=9.10.0`

Make sure you have installed `yarn`:

Later run: `yarn test` (runs tests with Jest)

## Ackownledgments

- [SuperTest](https://github.com/visionmedia/supertest)
- [moxios](https://github.com/axios/moxios)
