'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const mkdirp = require('mkdirp');
const path = require('path');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
    this.tmpDir = 'ozone-be-generator';
    this.configFiles = 'configFiles';
    this.gitpack = 'gitpack';
    this.tmpFolders = ['server'];
    this.appname = this.appname.replace(/\s+/g, '-');
  }

  prompting() {
    /* eslint-disable-line function-paren-newline */ this.log(
      yosay(
        `Welcome to the divine ${chalk.red('generator-ozone-be')} generator!`
      )
    ); /* eslint-disable-line function-paren-newline */

    const prompts = [
      {
        type: 'input',
        name: 'projectName',
        message: 'Your project name',
        default: this.appname.replace(/\s+/g, '-')
      }
    ];

    this.log(this.appname.replace(/\s+/g, '-') + ' is getting generated..!');

    return this.prompt(prompts).then(props => {
      this.props = props;
    });
  }

  copyDirectoryContents(folders = []) {
    folders.forEach(folder => {
      mkdirp.sync(this.destinationPath(), folder);
      this.fs.copy(
        this.templatePath(this.tmpDir + '/' + folder + '/*'),
        this.destinationPath(folder + '/')
      );
    });
  }

  writing() {
    this.copyDirectoryContents(this.tmpFolders);

    const configFilesPath = path.join(
      __dirname,
      '/templates/',
      this.tmpDir,
      this.configFiles,
      '/**'
    );

    this.fs.copy(configFilesPath, this.destinationPath(), {
      globOptions: { dot: true }
    });

    this.fs.copyTpl(
      this.templatePath(this.tmpDir + '/', this.gitpack, '/_package.json'),
      this.destinationPath('package.json'),
      {
        name: this.props.projectName
      }
    );

    this.fs.copy(
      this.templatePath(this.tmpDir + '/', this.gitpack, '/_gitignore'),
      this.destinationPath('.npmignore')
    );

    this.fs.copy(
      this.templatePath(this.tmpDir + '/', this.gitpack, '/_gitignore'),
      this.destinationPath('.gitignore')
    );
  }

  install() {
    this.installDependencies({
      bower: false,
      callback() {
        console.log('Dependencies installed. Enjoy, Happy Coding !');
        process.exit(1);
      }
    });
  }
};
